import React, {Component} from 'react';
import './ToDoApp.css';
import {connect} from "react-redux";
import {getPosts, sendPosts} from "../../store/action";

class ToDoApp extends Component {
    componentDidMount() {
        this.props.getPosts();
    }

    render() {
        return (
            <div className="container">
                <input type="text" onChange={this.props.handleChange}/>
                <button onClick={this.props.sendPosts}>Add</button>
                {this.props.posts.map((item, key) => <div className="post" key={key}>{item.value}</div>)}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        posts: state.posts,
        post: state.post
    }
};

const mapDispatchToProps = dispatch => {
    return {
        handleChange: event => dispatch({type: 'CHANGE', value: event.target.value}),
        sendPosts: () => dispatch(sendPosts()),
        getPosts: () => dispatch(getPosts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoApp);