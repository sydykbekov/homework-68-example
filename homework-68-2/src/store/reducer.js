import {FETCH_POSTS_SUCCESS} from "./action";

const initialState = {
    posts: [],
    post: ''
};

const reducer = (state = initialState, action) => {
    if (action.type === 'CHANGE') {
        return {
            ...state,
            post: action.value
        }
    }

    if (action.type === 'ADD') {
        return {posts: action.posts}
    }

    if (action.type === FETCH_POSTS_SUCCESS) {
        return {posts: action.posts};
    }
    return state;
};

export default reducer;