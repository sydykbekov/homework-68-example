import axios from 'axios';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';

export const fetchPostsSuccess = (posts) => {
    return {type: FETCH_POSTS_SUCCESS, posts: posts};
};

let fetchingPosts = (dispatch) => {
    axios.get('posts.json').then(response => {
        const posts = [];
        for (let key in response.data) {
            posts.push({...response.data[key], id: key});
        }
        dispatch(fetchPostsSuccess(posts));
    });
};

export const sendPosts = (post) => {
    return dispatch => {
        axios.post('posts.json', {value: post}).then(() => {
            fetchingPosts(dispatch);
        })
    }
};

export const getPosts = () => {
    return dispatch => {
        fetchingPosts(dispatch);
    }
};