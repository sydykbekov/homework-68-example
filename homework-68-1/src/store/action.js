import axios from '../axios-counter';
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';
export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';
export const SAVE_COUNTER_RESULT = 'SAVE_COUNTER_RESULT';

export const incrementCounter = (obj) => {
    return dispatch => {
        dispatch(saveCounterResult());
        axios.put('/counter.json', obj).then(response => {
            console.log(response);
        }, error => {
            dispatch(fetchCounterError());
        }).then(() => {
            fetchCounter();
        })
    }
};

export const decrementCounter = () => {
    return { type: DECREMENT };
};

export const addCounter = (amount) => {
    return { type: ADD, amount};
};

export const subtractCounter = (amount) => {
    return { type: SUBTRACT, amount};
};

export const fetchCounterRequest = () => {
    return { type: FETCH_COUNTER_REQUEST };
};

export const fetchCounterSuccess = (counter) => {
    return { type: FETCH_COUNTER_SUCCESS, counter: counter};
};

export const fetchCounterError = () => {
    return { type: FETCH_COUNTER_ERROR };
};

export const saveCounterResult = (counter) => {
    return { type: SAVE_COUNTER_RESULT, counter: counter};
};

export const fetchCounter = () => {
    return dispatch => {
        dispatch(fetchCounterRequest());
        axios.get('/counter.json').then(response => {
            dispatch(fetchCounterSuccess(response.data));
        }, error => {
            dispatch(fetchCounterError());
        });
    }
};

export const saveCounter = (obj) => {
    return dispatch => {
        dispatch(saveCounterResult());
        axios.put('/counter.json', obj).then(response => {
            console.log(response);
        }, error => {
            dispatch(fetchCounterError());
        })
    }
};
