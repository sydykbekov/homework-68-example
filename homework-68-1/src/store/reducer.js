import {INCREMENT, DECREMENT, SUBTRACT, ADD, FETCH_COUNTER_SUCCESS, SAVE_COUNTER_RESULT} from "./action";

const initialState = {
    counter: 333
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT:
            return {counter: state.counter + 1};
        case DECREMENT:
            return {counter: state.counter - 1};
        case ADD:
            return {counter: state.counter + action.amount};
        case SUBTRACT:
            return {counter: state.counter - action.amount};
        case FETCH_COUNTER_SUCCESS:
            return {counter: action.counter};
        case SAVE_COUNTER_RESULT:
            return {counter: action.counter};
        default:
            return state;
    }
};

export default reducer;
